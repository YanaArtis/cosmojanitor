﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Game UI manager.
  /// </summary>
  public class CanvasManager : MonoBehaviour
  {
    [SerializeField] private GameObject _goMainMenu;    // Main Menu window
    [SerializeField] private GameObject _goLevelTitle;  // Start New Level window
    [SerializeField] private GameObject _goGameHUD;     // Game Indicators window
    [SerializeField] private GameObject _goGameOver;    // Game Over window

    // Main Menu fields
    [SerializeField] private TMP_Text _txtMainMenuHiScoreTitle;
    [SerializeField] private TMP_Text _txtMainMenuHiScoreValue;
    // Start New Level window fields
    [SerializeField] private TMP_Text _txtLevelStart;
    // Game Indicator fields
    [SerializeField] private TMP_Text _txtHUDScore;
    [SerializeField] private TMP_Text _txtHUDHiScore;
    [SerializeField] private TMP_Text _txtHUDShields;
    [SerializeField] private TMP_Text _txtHUDLevel;
    [SerializeField] private TMP_Text _txtHUDRocks;
    // Game Over fields
    [SerializeField] private TMP_Text _txtNewHighScoreTitle;
    [SerializeField] private TMP_Text _txtNewHighScoreValue;

    // We use this class as singleton with static API methods
    private static CanvasManager _instance;

    private void Awake()
    {
      if (_instance != null)
      {
        Destroy(this);
        return;
      }
      _instance = this;
    }

    /// <summary>
    /// Hide all windows.
    /// </summary>
    public static void HideAll ()
    {
      if (_instance == null)
      {
        return;
      }
      _instance._goMainMenu.SetActive(false);
      _instance._goLevelTitle.SetActive(false);
      _instance._goGameHUD.SetActive(false);
      _instance._goGameOver.SetActive(false);
    }

    /// <summary>
    /// Show Main Menu window and show best score if it is > than 0.
    /// </summary>
    /// <param name="bestScore">Best score value</param>
    public static void ShowMainMenu (int bestScore = 0)
    {
      if (_instance == null)
      {
        return;
      }
      HideAll();
      _instance._goMainMenu.SetActive(true);
      if (bestScore == 0)
      {
        _instance._txtMainMenuHiScoreTitle.gameObject.SetActive(false);
        _instance._txtMainMenuHiScoreValue.gameObject.SetActive(false);
      }
      else
      {
        _instance._txtMainMenuHiScoreValue.text = bestScore.ToString();
        _instance._txtMainMenuHiScoreTitle.gameObject.SetActive(true);
        _instance._txtMainMenuHiScoreValue.gameObject.SetActive(true);
      }
    }

    /// <summary>
    /// Show Start New Level window
    /// </summary>
    /// <param name="levelN">Level number</param>
    public static void ShowLevelTitle (int levelN)
    {
      if (_instance == null)
      {
        return;
      }
      HideAll();
      _instance._goLevelTitle.SetActive(true);
      _instance._txtLevelStart.text = levelN.ToString();
    }

    /// <summary>
    /// Show Game Indicators window with specified indicators values.
    /// </summary>
    /// <param name="levelN">Current level number</param>
    /// <param name="score">Current player score</param>
    /// <param name="hiScore">Best score in current app session</param>
    /// <param name="shields">Player's ship shields value</param>
    /// <param name="rocks">Rocks on the game field</param>
    public static void ShowGameHUD (int levelN, int score, int hiScore, int shields, int rocks)
    {
      if (_instance == null)
      {
        return;
      }
      HideAll();
      _instance._goGameHUD.SetActive(true);
      _instance._txtHUDLevel.text = levelN.ToString();
      _instance._txtHUDScore.text = score.ToString();
      _instance._txtHUDHiScore.text = hiScore.ToString();
      _instance._txtHUDShields.text = shields.ToString();
      _instance._txtHUDRocks.text = rocks.ToString();
    }

    /// <summary>
    /// Set score indicator on Game Indicators window.
    /// </summary>
    /// <param name="newScore">New player's score</param>
    public static void SetGameHUDScore (int newScore)
    {
      _instance._txtHUDScore.text = newScore.ToString();
    }
    /// <summary>
    /// Set shields indicator on Game Indicators window.
    /// </summary>
    /// <param name="newShields">New shields value</param>
    public static void SetGameHUDShields (int newShields)
    {
      _instance._txtHUDShields.text = newShields.ToString();
    }

    /// <summary>
    /// Set rocks indicator on Game Indicators window.
    /// </summary>
    /// <param name="newRocks">New rocks quantity</param>
    public static void SetGameHUDRocks (int newRocks)
    {
      _instance._txtHUDRocks.text = newRocks.ToString();
    }

    /// <summary>
    /// Show Game Over window with 'new high score' message score
    ///  specified is > 0.
    /// </summary>
    /// <param name="newHiScore"></param>
    public static void ShowGameOver (int newHiScore = 0)
    {
      if (_instance == null)
      {
        return;
      }
      HideAll();
      _instance._goGameOver.SetActive(true);
      if (newHiScore == 0)
      {
        _instance._txtNewHighScoreTitle.gameObject.SetActive(false);
        _instance._txtNewHighScoreValue.gameObject.SetActive(false);
      }
      else
      {
        _instance._txtNewHighScoreValue.text = newHiScore.ToString();
        _instance._txtNewHighScoreTitle.gameObject.SetActive(true);
        _instance._txtNewHighScoreValue.gameObject.SetActive(true);
      }
    }

    /// <summary>
    /// Start new game
    /// </summary>
    public void OnPlayButtonPressed ()
    {
      GameManager.StartNewGame();
    }
  }
}
