﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Pool of explosions.
  /// </summary>
  public class ExplosionPool : ObjectsPool<Explosion>
  {
    // We use this class as singleton with static API methods
    private static ExplosionPool _instance;

    private void Awake()
    {
      if (_instance != null)
      {
        Destroy(this);
        return;
      }
      _instance = this;
    }

    /// <summary>
    /// Return free object from the pool
    ///  (adding the new one if required).
    /// </summary>
    /// <returns>Object from the pool</returns>
    public static Explosion Get()
    {
      if (_instance == null)
      {
        return null;
      }
      return _instance.GetObject();
    }

    /// <summary>
    /// Put the object in pull and de-activate it.
    /// </summary>
    /// <param name="explosion">Object to put in the pool</param>
    public static void ReturnToPool(Explosion explosion)
    {
      if (_instance != null)
      {
        _instance.ReturnObjectToPool(explosion);
      }
    }

    /// <summary>
    /// Put all Explosion objects in the pool.
    /// </summary>
    public static void ReturnAllToPool()
    {
      if (_instance != null)
      {
        _instance.ReturnAllObjectsToPool();
      }
    }
  }
}
