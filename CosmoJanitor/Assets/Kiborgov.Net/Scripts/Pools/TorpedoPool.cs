﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Pool of torpedos.
  /// </summary>
  public class TorpedoPool : ObjectsPool<Torpedo>
  {
    // We use this class as singleton with static API methods
    private static TorpedoPool _instance;

    private void Awake()
    {
      if (_instance != null)
      {
        Destroy(this);
        return;
      }
      _instance = this;
    }

    /// <summary>
    /// Return free object from the pool
    ///  (adding the new one if required).
    /// </summary>
    /// <returns>Object from the pool</returns>
    public static Torpedo Get ()
    {
      if (_instance == null)
      {
        return null;
      }
      return _instance.GetObject();
    }

    /// <summary>
    /// Put the object in pull and de-activate it.
    /// </summary>
    /// <param name="torpedo">Object to put in the pool</param>
    public static void ReturnToPool (Torpedo torpedo)
    {
      if (_instance != null)
      {
        _instance.ReturnObjectToPool(torpedo);
      }
    }

    /// <summary>
    /// Put all Torpedo objects in the pool.
    /// </summary>
    public static void ReturnAllToPool()
    {
      if (_instance != null)
      {
        _instance.ReturnAllObjectsToPool();
      }
    }
  }
}
