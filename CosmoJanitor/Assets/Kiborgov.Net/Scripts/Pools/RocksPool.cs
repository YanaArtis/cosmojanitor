﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Pool of asteroids.
  /// </summary>
  public class RocksPool : ObjectsPool<Rock>
  {
    // We use this class as singleton with static API methods
    private static RocksPool _instance;

    private void Awake()
    {
      if (_instance != null)
      {
        Destroy(this);
        return;
      }
      _instance = this;
    }

    /// <summary>
    /// Return free object from the pool
    ///  (adding the new one if required).
    ///  Setup the asteroid accordingly
    ///  current level settings.
    /// </summary>
    /// <returns>Object from the pool</returns>
    public static Rock Get()
    {
      if (_instance == null)
      {
        return null;
      }
      Rock rock = _instance.GetObject();

      float x;
      float y;
      if (Random.value < 0.5f)
      {
        x = Random.Range(-GameCamera.border, GameCamera.border);
        y = (Random.value < 0.5f) ? -GameCamera.border : GameCamera.border;
      }
      else
      {
        y = Random.Range(-GameCamera.border, GameCamera.border);
        x = (Random.value < 0.5f) ? -GameCamera.border : GameCamera.border;
      }

      rock.Setup(new Vector3(x, y, 0f), 3, 50);
      rock.Aim(GameManager.GetPlayerPosition(), GameManager.GetRockAimingError());
      rock.gameObject.SetActive(true);
      return rock;
    }

    /// <summary>
    /// Get set of asteroids one by one, with random delay between
    ///  asteroids adding.
    /// </summary>
    /// <param name="numOfRocks">Number of asteroid to add to the game
    ///   field</param>
    /// <param name="minDelay">Minimal delay between asteroids
    ///   adding</param>
    /// <param name="maxDelay">Maximal delay between asteroids
    ///   adding</param>
    public static void GetSet (int numOfRocks, float minDelay = 1f, float maxDelay = 1f)
    {
      if (_instance != null)
      {
        _instance.StartCoroutine
          (_instance.GetSetCoroutine(numOfRocks, minDelay, maxDelay));
      }
    }

    /// <summary>
    /// Coroutine to add the set of asteroids.
    /// </summary>
    /// <param name="numOfRocks">Number of asteroid to add to the game
    ///   field</param>
    /// <param name="minDelay">Minimal delay between asteroids
    ///   adding</param>
    /// <param name="maxDelay">Maximal delay between asteroids
    ///   adding</param>
    /// <returns></returns>
    public IEnumerator GetSetCoroutine (int numOfRocks, float minDelay, float maxDelay)
    {
      for (int i = 0; i < numOfRocks; i++)
      {
        Get();
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
      }
    }

    /// <summary>
    /// Put the object in pull and de-activate it.
    /// </summary>
    /// <param name="rock">Object to put in the pool</param>
    public static void ReturnToPool(Rock rock)
    {
      if (_instance != null)
      {
        _instance.ReturnObjectToPool(rock);
      }
    }

    /// <summary>
    /// Put all Rock objects in the pool.
    /// </summary>
    public static void ReturnAllToPool()
    {
      if (_instance != null)
      {
        _instance.ReturnAllObjectsToPool();
      }
    }
  }
}
