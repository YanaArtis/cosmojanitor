﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Base class for object pools.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class ObjectsPool<T> : MonoBehaviour where T : Component
  {
    [SerializeField]
    private GameObject goPrefab; // Prefab for pool objects creation

    private Queue<T> queueOfInactiveObjs = new Queue<T>();  // Inactive objects queue
    private List<T> listOfActiveObjs = new List<T>();  // Active objects list

    /// <summary>
    /// Get object from the pool (or create new if required).
    /// </summary>
    /// <returns>Available game object</returns>
    public T GetObject ()
    {
      if (queueOfInactiveObjs.Count == 0)
      {
        AddObjects(1);
      }

      T obj = queueOfInactiveObjs.Dequeue();
      listOfActiveObjs.Add(obj);
      return obj;
    }

    /// <summary>
    /// Create new objects to enlarge the pool
    /// </summary>
    /// <param name="count">Number of objects to create</param>
    private void AddObjects (int count)
    {
      for (int i = 0; i < count; i++)
      {
        GameObject go = Instantiate(goPrefab);
        go.transform.SetParent(transform);
        go.SetActive(false);
        queueOfInactiveObjs.Enqueue(go.GetComponent<T>());
      }
    }

    /// <summary>
    /// Put the object back to the pool after object's usage is over
    /// </summary>
    /// <param name="go">Object to put back in the pool</param>
    public void ReturnObjectToPool (T obj)
    {
      listOfActiveObjs.Remove(obj);
      obj.gameObject.SetActive(false);
      queueOfInactiveObjs.Enqueue(obj);
    }

    /// <summary>
    /// Return all objects to pool
    /// </summary>
    public void ReturnAllObjectsToPool ()
    {
      while (listOfActiveObjs.Count > 0)
      {
        T obj = listOfActiveObjs[0];
        ReturnObjectToPool(obj);
      }
    }
  }
}
