﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Player's ship torpedo launcher.
  /// </summary>
  public class TorpedoLauncher : MonoBehaviour
  {
    public Transform tFirePoint; // Fire end of the barrel

    private static float _cooldownDelay = 2f; // Cooldown time

    private float _cooldownTimer = 0f; // Timer to check cooldown
    private bool _canFire = true; // Can torpedo launcher fire now?

    // Amount of maximum stored fire commands
    private const int MAX_FIRE_COMMANDS_NUM = 1000;
    // Storage for dtored fire commands
    private QueueFixedCapacity<Vector3> _fireCommands =
      new QueueFixedCapacity<Vector3>(MAX_FIRE_COMMANDS_NUM);

    /// <summary>
    /// Reset torpedo launcher to initial state.
    /// </summary>
    public void Reset()
    {
      _cooldownTimer = 0f;
      _canFire = true;
      _fireCommands.Clear();
    }

    void Update()
    {
      if (!_canFire)
      {
        _cooldownTimer += Time.deltaTime;
        if (_cooldownTimer >= _cooldownDelay)
        {
          _canFire = true;
        }
      }
      if (_canFire && !_fireCommands.IsEmpty())
      {
        Fire(_fireCommands.Dequeue());
      }
    }

    /// <summary>
    /// Put new fire command in fire commands storage.
    /// </summary>
    /// <param name="targetPosition">Aiming position</param>
    public void CreateFireCommand (Vector3 targetPosition)
    {
      if (_fireCommands.IsFull())
      {
        _fireCommands.Dequeue();
      }
      _fireCommands.Enqueue(targetPosition);
    }

    /// <summary>
    /// Launch missile do the specified position.
    /// </summary>
    /// <param name="targetPosition">Aiming position</param>
    private void Fire (Vector3 targetPosition)
    {
      transform.LookAt(targetPosition);

      var shot = TorpedoPool.Get();
      shot.transform.position = tFirePoint.position;
      shot.transform.rotation = tFirePoint.rotation;
      shot.gameObject.SetActive(true);
      _cooldownTimer = 0;
      _canFire = false;
    }

    /// <summary>
    /// Set torpedo launcher cooldown time.
    /// </summary>
    /// <param name="cooldownTime">New cooldown time</param>
    public static void SetCooldownTime (float cooldownTime)
    {
      _cooldownDelay = cooldownTime;
    }
  }
}
