﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Player's ship.
  /// </summary>
  public class Player : MonoBehaviour
  {
    // Hit points required to destroy player's ship
    public int healthPoints = 16;
    public TorpedoLauncher torpedoLauncher; // Torpedo launcher
    // Shields rotating around the player's ship (just for visual)
    public PlayerShield[] shields;

    // Coefficient for calculation mouse click position projection to
    //  game field.
    private float _screenToWorldCoef;

    void Start()
    {
      Vector3 camPos = Camera.main.transform.position;
      _screenToWorldCoef = 2 * GameCamera.border / Screen.width;
    }

    // TODO: move Update to coroutine and remove GameManager.CanUserPlay()
    void Update()
    {
      if (GameManager.CanUserPlay() && Input.GetMouseButtonDown(0))
      {
        Vector3 v = Input.mousePosition;
        v.x = (v.x - Screen.width / 2) * _screenToWorldCoef;
        v.y = (v.y - Screen.height / 2) * _screenToWorldCoef;
        v.z = 0;
        torpedoLauncher.CreateFireCommand(v);
      }
    }

    /// <summary>
    /// Reset player's ship to the initial state.
    /// </summary>
    public void Reset()
    {
      gameObject.SetActive(true);
      foreach (PlayerShield shield in shields)
      {
        shield.SetBallsNum(8);
      }
      healthPoints = 16;
      torpedoLauncher.Reset();
    }

    /// <summary>
    /// Hit player by rock with specified hit points.
    /// </summary>
    /// <param name="hitPoints">Hit points</param>
    public void Hit (int hitPoints)
    {
      healthPoints -= hitPoints;

      Explosion explosion = ExplosionPool.Get();
      explosion.transform.position = transform.position - transform.forward;
      explosion.gameObject.SetActive(true);

      if (healthPoints <= 0)
      {
        GameManager.OnPlayerHit();
        gameObject.SetActive(false);
        return;
      }
      int n = Random.Range(0, 2);
      int h = healthPoints / 2;
      shields[n].SetBallsNum(h);
      shields[n].InverseRotation();
      n = 1 - n;
      shields[n].SetBallsNum(healthPoints - h);
      shields[n].InverseRotation();

      GameManager.OnPlayerHit();
    }

    /// <summary>
    /// Check for hit with the rock, and process this.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
      Rock rock = other.GetComponent<Rock>();
      if (rock != null)
      {
        rock.ReturnToPool();
        Hit(1);
      }
    }

    /// <summary>
    /// Return player's health points.
    /// </summary>
    /// <returns>Player health points</returns>
    public int GetHealthPoints ()
    {
      return healthPoints;
    }
  }
}
