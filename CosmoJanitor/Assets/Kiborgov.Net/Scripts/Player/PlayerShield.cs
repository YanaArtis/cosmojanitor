﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Ring with balls-shields that rotates around player's ship.
  /// </summary>
  public class PlayerShield : MonoBehaviour
  {
    public float rotationSpeed; // Rotation speed of shields ring

    public Transform[] balls;   // Balls in the shields ring

    private int activeShieldsN = 8; // Number of active balls

    private void Awake()
    {
      SetBallsNum(balls.Length);
    }

    /// <summary>
    /// Set number of active balls.
    /// </summary>
    /// <param name="n">Number of active balls</param>
    public void SetBallsNum (int n)
    {
      activeShieldsN = n;
      int i = 0;
      if (n > 0)
      {
        float angle = balls[0].eulerAngles.z;
        float angleStep = 360f / n;
        for (i = 1; (i < n) && (i < balls.Length); i++)
        {
          balls[i].gameObject.SetActive(true);
          angle += angleStep;
          balls[i].eulerAngles = new Vector3(0, 0, angle);
        }
      }
      for (; i < balls.Length; i++)
      {
        balls[i].gameObject.SetActive(false);
      }
    }

    /// <summary>
    /// Decrease number of active balls by 1.
    /// </summary>
    public void DecreaseBallsNum ()
    {
      SetBallsNum(activeShieldsN - 1);
    }

    /// <summary>
    /// Inverse shield ring rotation.
    /// </summary>
    public void InverseRotation ()
    {
      rotationSpeed = -rotationSpeed;
    }

    void FixedUpdate()
    {
      if (activeShieldsN > 0)
      {
        float z = transform.eulerAngles.z + Time.fixedDeltaTime *
          rotationSpeed / (activeShieldsN * activeShieldsN);
        transform.eulerAngles = new Vector3(0, 0, z);
      }
    }
  }
}
