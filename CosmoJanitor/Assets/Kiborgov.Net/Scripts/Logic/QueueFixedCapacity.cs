﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Queue of objects T with fixed capacity
  /// </summary>
  /// <typeparam name="T">Type of queued objects</typeparam>
  public class QueueFixedCapacity<T> where T : struct
  {
    private int _currentSize;
    /// <summary>
    /// Get number of queued objects
    /// </summary>
    public int Count
    {
      get { return _currentSize; }
    }

    private T[] _arr;     // Array to store queued objects
    private int _headNdx; // Array index of head queue element
    private int _tailNdx; // Array index of tail queue element

    /// <summary>
    /// Create queue with fixed capacity
    /// </summary>
    /// <param name="maxCapacity">Queue capacity</param>
    public QueueFixedCapacity(int maxCapacity)
    {
      if (maxCapacity < 1)
      {
        maxCapacity = 10;
      }
      _arr = new T[maxCapacity];
      Clear();
    }

    /// <summary>
    /// Set queue empty
    /// </summary>
    public void Clear ()
    {
      _headNdx = 0;
      _tailNdx = -1;
      _currentSize = 0;
    }

    /// <summary>
    /// Put new element in queue' tail.
    /// </summary>
    /// <param name="value">Element to put in queue</param>
    public void Enqueue (T value)
    {
      if (_currentSize >= _arr.Length)
      {
        throw new System.InvalidOperationException("Cannot enqueue: queue is full.");
      }
      ++_currentSize;
      if (++_tailNdx >= _arr.Length)
      {
        _tailNdx = 0;
      }
      _arr[_tailNdx] = value;
    }

    /// <summary>
    /// Remove head element from queue, and return it to method's caller.
    /// </summary>
    public T Dequeue ()
    {
      if (_currentSize < 1)
      {
        throw new System.InvalidOperationException("Cannot dequeue: queue is empty.");
      }
      T retValue = _arr[_headNdx];
      if (++_headNdx >= _arr.Length)
      {
        _headNdx = 0;
      }
      --_currentSize;
      return retValue;
    }
    
    /// <summary>
    /// Is queue empty?
    /// </summary>
    /// <returns>True, if queue is empty</returns>
    public bool IsEmpty ()
    {
      return (_currentSize < 1);
    }

    /// <summary>
    /// Is queue full?
    /// </summary>
    /// <returns>True, if queue is full</returns>
    public bool IsFull ()
    {
      return (_currentSize >= _arr.Length);
    }
  }
}
