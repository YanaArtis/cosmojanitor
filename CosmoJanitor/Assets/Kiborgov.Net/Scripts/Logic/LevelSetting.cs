﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Storage object for level settings.
  /// </summary>

  //  [System.Serializable]
  [CreateAssetMenu(menuName = "CosmoJanitor/Level", fileName = "New Level")]
  public class LevelSetting : ScriptableObject
  {
    [Tooltip("Number of rocks (min, max)")]
    [SerializeField] private Vector2Int _rocksNum;
    public Vector2Int rocksNum
    {
      get { return _rocksNum; }
      protected set { }
    }

    [Tooltip("Hits required to destroy the rock (min, max)")]
    [SerializeField] private Vector2Int _rocksHP;
    public Vector2Int rocksHP
    {
      get { return _rocksHP; }
      protected set { }
    }

    [Tooltip("Rock move speed range (min, max)")]
    [SerializeField] private Vector2 _rockSpeedRange;
    public Vector2 rockSpeedRange
    {
      get { return _rockSpeedRange; }
      protected set { }
    }

    [Tooltip("Rock rotation speed range (min, max) - just for the visual")]
    [SerializeField] private Vector2 _rockRotationSpeedRange;
    public Vector2 rockRotationSpeedRange
    {
      get { return _rockRotationSpeedRange; }
      protected set { }
    }

    [Tooltip("How precisely rocks aiming to player at the move start, from 0 (best accuracy) to positive infinity (the bigger number - the worse accuracy)")]
    [SerializeField] private float _rockAimingError;
    public float rockAimingError
    {
      get { return _rockAimingError; }
      protected set { }
    }
  }
}
