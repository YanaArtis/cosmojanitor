﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Game logic implementation.
  /// </summary>
  public class GameManager : MonoBehaviour
  {
    [Tooltip("Current level")]
    [SerializeField] private int _currentLevel;

    [Tooltip("Level settings")]
    [SerializeField] private List<LevelSetting> _levelSettings;

    [Tooltip("Torpedo move speed")]
    [SerializeField] private float _torpedoSpeed;

    [Tooltip("Torpedo max distance")]
    [SerializeField] private float _torpedoMaxDistance;

    [Tooltip("Torpedo launcher cooldown time")]
    [SerializeField] private float _torpedoLauncherCooldownTime;

    [Tooltip("Score for rick hit")]
    [SerializeField] private int _scoreForRock;

    [Tooltip("Start level screen delay")]
    [SerializeField] private int _startLevelScreenDelay;

    [Tooltip("Game Over screen delay")]
    [SerializeField] private int _gameOverScreenDelay;

    [SerializeField] private Player _player;  // Player's space ship

    private static GameManager _instance;

    private LevelSetting _currentSettings;  // Current level settings
    private bool _canUserPlay = false;      // Can user fire now

    private int _bestScore = 0; // Best score for all games in this application session
    private int _score = 0;     // Current player score
    private int _rocksNum = 0;  // Number of rocks on the game field

    // Game states
    public enum State { None, MainMenu, NewLevel, LevelPlay, GameOver }
    private State _state = State.None; // Current game state

    private void Awake()
    {
      if (_instance != null)
      {
        Destroy(this);
        return;
      }
      _instance = this;

      Torpedo.SetMoveSpeed(_torpedoSpeed);
      Torpedo.SetMaxDistance(_torpedoMaxDistance);

      TorpedoLauncher.SetCooldownTime(_torpedoLauncherCooldownTime);
    }

    void Start ()
    {
      SetState(State.MainMenu);
    }

    /// <summary>
    /// Change game state.
    /// </summary>
    /// <param name="newState">Game state identifier</param>
    public void SetState (State newState)
    {
      switch (newState)
      {
        case State.MainMenu:
          _canUserPlay = false;
          CanvasManager.ShowMainMenu(_bestScore);
          break;
        case State.NewLevel:
          InitializeGameWithLevelSettings(_currentLevel);
          CanvasManager.ShowLevelTitle(_currentLevel + 1);
          _canUserPlay = false;
          StartCoroutine(WaitAndSetNewState(_startLevelScreenDelay, State.LevelPlay));
          break;
        case State.LevelPlay:
          CanvasManager.ShowGameHUD(_currentLevel + 1, _score, _bestScore, _player.GetHealthPoints(), _rocksNum);
          _canUserPlay = true;
          break;
        case State.GameOver:
          _canUserPlay = false;
          int newHighScore = 0;
          if (_score > _bestScore)
          {
            newHighScore = _bestScore = _score;
          }
          CanvasManager.ShowGameOver(newHighScore);
          StartCoroutine(WaitAndSetNewState(_gameOverScreenDelay, State.MainMenu));
          break;
      }
      _state = newState;
    }

    /// <summary>
    /// Set new state after some delay.
    /// </summary>
    /// <param name="delay">Delay in sec</param>
    /// <param name="newState">New game state</param>
    /// <returns></returns>
    private IEnumerator WaitAndSetNewState (float delay, State newState)
    {
      yield return new WaitForSeconds(delay);
      SetState(newState);
    }

    /// <summary>
    /// Start new game: reset all game objects, score and level then start new game session.
    /// </summary>
    private void _StartNewGame ()
    {
      _score = 0;

      _player.Reset();

      RocksPool.ReturnAllToPool();
      TorpedoPool.ReturnAllToPool();
      ExplosionPool.ReturnAllToPool();
      _currentLevel = 0;
      SetState(State.NewLevel);
    }
    
    /// <summary>
    /// Static interface function to be called from menu.
    /// </summary>
    public static void StartNewGame ()
    {
      if (_instance != null)
      {
        _instance._StartNewGame();
      }
    }
    
    /// <summary>
    /// Initialize game with specified level presets.
    /// </summary>
    /// <param name="levelN">Level number</param>
    private void InitializeGameWithLevelSettings (int levelN)
    {
      if (levelN < 0)
      {
        return;
      }
      _currentLevel = levelN;
      int additionalRocks = 0;
      if (_currentLevel < _levelSettings.Count)
      {
        _currentSettings = _levelSettings[_currentLevel];
      }
      else
      {
        _currentSettings = _levelSettings[_levelSettings.Count - 1];
        additionalRocks = (_levelSettings.Count - _currentLevel + 1) * 10;
      }

      _rocksNum = Random.Range
        (_currentSettings.rocksNum.x + additionalRocks,
        _currentSettings.rocksNum.y + additionalRocks);
      RocksPool.GetSet(_rocksNum);
    }

    /// <summary>
    /// Get player position on game field.
    /// </summary>
    /// <returns></returns>
    public static Vector3 GetPlayerPosition ()
    {
      if ((_instance == null) && (_instance._player == null))
      {
        return Vector3.zero;
      }
      return _instance._player.transform.position;
    }

    /// <summary>
    /// Get value describes how precise rocks aimed to Player on current level.
    /// </summary>
    /// <returns>Rock aiming error for current level</returns>
    public static float GetRockAimingError ()
    {
      if ((_instance == null) || (_instance._currentSettings == null))
      {
        return 0f;
      }
      return _instance._currentSettings.rockAimingError;
    }

    /// <summary>
    /// Can player fire?
    /// </summary>
    /// <returns>True if player can fire</returns>
    public static bool CanUserPlay ()
    {
      return (_instance == null) ? false : _instance._canUserPlay;
    }

    /// <summary>
    /// Check is the current level passed. And start next level, if yes.
    /// </summary>
    private void CheckForNewLevel ()
    {
      if (_rocksNum < 1)
      {
        ++_currentLevel;
        SetState(State.NewLevel);
      }
    }

    /// <summary>
    /// Check is current game over. And terminate game, if yes.
    /// </summary>
    private void CheckForGameOver ()
    {
      if (_player.GetHealthPoints() <= 0)
      {
        SetState(State.GameOver);
      }
    }

    /// <summary>
    /// Make all required logic checks when torpedo hits player.
    /// </summary>
    public static void OnRockHit()
    {
      if (_instance != null)
      {
        _instance._score += _instance._scoreForRock;
        --_instance._rocksNum;
        CanvasManager.SetGameHUDScore(_instance._score);
        CanvasManager.SetGameHUDRocks(_instance._rocksNum);
        _instance.CheckForNewLevel();
      }
    }

    /// <summary>
    /// Make all required logic checks when rock hits player.
    /// </summary>
    public static void OnPlayerHit()
    {
      if ((_instance != null) && (_instance._player != null))
      {
        --_instance._rocksNum;
        CanvasManager.SetGameHUDShields(_instance._player.GetHealthPoints());
        CanvasManager.SetGameHUDRocks(_instance._rocksNum);
        _instance.CheckForGameOver();
        _instance.CheckForNewLevel();
      }
    }
  }
}
