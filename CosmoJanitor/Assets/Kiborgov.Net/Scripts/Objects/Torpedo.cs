﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Torpedo player can launch via torpedo launcher to hit asteroids.
  /// </summary>
  public class Torpedo : MonoBehaviour
  {
    private static float _moveSpeed = 3f;   // Torpedo move speed
    private static float _maxDistance = 5f; // Torpedo move range

    private Vector3 _startPosition;         // Torpedo move start
                                            //  position, required to
                                            //  check the range.

    private void OnEnable ()
    {
      _startPosition = transform.position;
    }

    void FixedUpdate()
    {
      transform.Translate(Vector3.forward * _moveSpeed * Time.fixedDeltaTime);
      if (Vector3.Distance(transform.position, _startPosition) >= _maxDistance)
      {
        TorpedoPool.ReturnToPool(this);
      }
    }

    /// <summary>
    /// Return torpedo to its pool.
    /// </summary>
    public void ReturnToPool()
    {
      TorpedoPool.ReturnToPool(this);
    }

    /// <summary>
    /// Check for hit with the rock, and process this.
    /// </summary>
    /// <param name="other">Colliding object</param>
    public void OnTriggerEnter(Collider other)
    {
      Rock rock = other.GetComponent<Rock>();
      if (rock != null)
      {
        Explosion explosion = ExplosionPool.Get();
        explosion.transform.position = transform.position;
        explosion.gameObject.SetActive(true);
        rock.ReturnToPool();
        GameManager.OnRockHit();
        ReturnToPool();
      }
    }

    /// <summary>
    /// Set torpedo move speed.
    /// </summary>
    /// <param name="moveSpeed">New move speed</param>
    public static void SetMoveSpeed(float moveSpeed)
    {
      _moveSpeed = moveSpeed;
    }

    /// <summary>
    /// Set torpedo move range.
    /// </summary>
    /// <param name="distance">New move range</param>
    public static void SetMaxDistance(float distance)
    {
      _maxDistance = distance;
    }
  }
}
