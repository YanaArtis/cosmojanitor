﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Explosion animation. Appears when asteroid collides with torpedo
  ///  or player's ship.
  /// </summary>
  public class Explosion : MonoBehaviour
  {
    private float _lifeTime;  // Explosion's life time since it was released from pool
    private const float MAX_LIFE_TIME = 1f;  // Explosion's duration
    private WaitForSeconds _waitForLifeTime = new WaitForSeconds(MAX_LIFE_TIME);

    private void OnEnable()
    {
      _lifeTime = 0;
      StartCoroutine(SelfDestroyAfterLifeTime_Coroutine());
    }

    /// <summary>
    /// Wait for life time, then return the object back to pool.
    /// </summary>
    /// <returns></returns>
    private IEnumerator SelfDestroyAfterLifeTime_Coroutine ()
    {
      yield return _waitForLifeTime;
      ReturnToPool();
    }

    /// <summary>
    /// Return object back to the pool.
    /// </summary>
    public void ReturnToPool()
    {
      ExplosionPool.ReturnToPool(this);
    }
  }
}
