﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Asteroids, danger for player.
  /// </summary>
  public class Rock : MonoBehaviour
  {
    public GameObject[] goAvatars;    // Set of different rock models
    public Transform tContentHolder;  // Container holding rock models

    private float _moveSpeed;         // Rock move speed
    private Vector3 _rotationDeltas;  // Angular rotation speed
    private int _healthPoints;        // How many hit points required
                                      //  for rock destruction

    /// <summary>
    /// Set rock position, move speed, rotation speed and health points.
    /// Choose random rock 3D model.
    /// </summary>
    /// <param name="pos">Position</param>
    /// <param name="moveSpeed">Move speed</param>
    /// <param name="rotationSpeed">Angular rotation speed</param>
    /// <param name="hp">Health points (1 by default)</param>
    public void Setup(Vector3 pos, float moveSpeed, float rotationSpeed, int hp = 1)
    {
      transform.position = pos;
      _moveSpeed = moveSpeed;
      _rotationDeltas = Random.onUnitSphere * rotationSpeed;
      _healthPoints = hp;
      SetRandomAvatar();
    }

    void FixedUpdate ()
    {
      transform.position += transform.forward * _moveSpeed * Time.fixedDeltaTime;
      tContentHolder.eulerAngles += _rotationDeltas * Time.fixedDeltaTime;

      if (IsOutOfGameField(transform.position))
      {
        AimToPlayer();
      }
    }

    /// <summary>
    /// Make all rock 3D models invisible
    /// </summary>
    private void HideAllAvatars ()
    {
      foreach (GameObject avatar in goAvatars)
      {
        avatar.SetActive(false);
      }
    }


    /// <summary>
    /// Choose 3D model for this rock
    /// </summary>
    /// <param name="n">3D model index</param>
    public void SetAvatar (int n)
    {
      if ((n < 0) || (n >= goAvatars.Length))
      {
        return;
      }
      HideAllAvatars();
      goAvatars[n].SetActive(true);
    }

    /// <summary>
    /// Choose random rock 3D model.
    /// </summary>
    public void SetRandomAvatar ()
    {
      SetAvatar(Random.Range(0, goAvatars.Length));
    }

    /// <summary>
    /// Hit the rock with specified hit points amount.
    /// </summary>
    /// <param name="hitPoints">Hit points amount</param>
    public void Hit (int hitPoints)
    {
      _healthPoints -= hitPoints;
      if (_healthPoints < 0)
      {
        RocksPool.ReturnToPool(this);
      }
    }

    /// <summary>
    /// Return rock to its pool.
    /// </summary>
    public void ReturnToPool ()
    {
      RocksPool.ReturnToPool(this);
    }

    /// <summary>
    /// Set rock motion vector toward to the specified position with some error.
    /// </summary>
    /// <param name="aimTargetPosition">Goal position to move</param>
    /// <param name="aimError">Goal position will be shifted to random
    /// value within this error range.</param>
    public void Aim (Vector3 aimTargetPosition, float aimError)
    {
      Vector3 directionToPlayer = aimTargetPosition - transform.position;
      Vector2 perpendicularForDirectionToPlayer = Vector2.Perpendicular
        (new Vector2(directionToPlayer.x, directionToPlayer.y));
      Vector3 vErr = perpendicularForDirectionToPlayer *
        Random.Range(-aimError, aimError);
      Vector3 aimPoint = aimTargetPosition + new Vector3(vErr.x, vErr.y, 0);

      transform.LookAt(aimPoint);
    }

    /// <summary>
    /// Set rock motion vector toward to the player's ship with
    /// some error.
    /// </summary>
    public void AimToPlayer ()
    {
      Aim(GameManager.GetPlayerPosition(), GameManager.GetRockAimingError());
    }

    /// <summary>
    /// Check, is specified positin out of game field.
    /// </summary>
    /// <param name="pos">Position to check</param>
    /// <returns>True, if the position is outside the game field</returns>
    private bool IsOutOfGameField (Vector3 pos)
    {
      return
        (pos.x < -GameCamera.border)  ||
        (pos.x > GameCamera.border)   ||
        (pos.y < -GameCamera.border)  ||
        (pos.y > GameCamera.border);
    }
  }
}
