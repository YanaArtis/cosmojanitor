﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net.kiborgov.CosmoJanitor
{
  /// <summary>
  /// Helper procedures
  /// </summary>
  public class GameCamera
  {
    private static float _border = 0; // Distance to game field edge

    /// <summary>
    /// Distance to screen edge
    /// </summary>
    public static float border
    {
      get
      {
        if (_border == 0)
        {
          var cam = Camera.main;
          _border = cam.aspect * cam.orthographicSize;
        }
        return _border;
      }
      private set { }
    }
  }
}
